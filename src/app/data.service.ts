import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CardData} from './models/card-data';

@Injectable({
  providedIn: 'root'
})
export default class DataService {
  readonly cardUrl = "http://localhost:8080/api/card";
  subscription: Observable<Array<CardData>>;


  constructor(private http: HttpClient) {
    this.subscription = http.get<Array<CardData>>('assets/card-data.json');
  }

  public getCardData(): Observable<Array<CardData>> {
    return this.subscription;
  }

  submitCard(card: CardData): Observable<CardData> {
    return this.http.post<CardData>(this.cardUrl, card);
  }
}


