import {SortPipe} from './sort.pipe';
import {CardData} from "../models/card-data";

describe('SortPipe', () => {
  it('should create an instance', () => {
    const pipe = new SortPipe();
    expect(pipe).toBeTruthy();
  });
});

describe('SortPipe', () => {
  const pipe = new SortPipe();
  const initCardData: CardData[] = [{
    playerName: 'Dreamlurk The Unstoppable',
    realName: 'Brianna Forbes',
    asset: 'Foghammer Lead',
  },
    {playerName: 'Crystaldash', realName: 'Darcy Candice Ball', asset: 'Secret Glowquake Gold',},
    {playerName: 'Speedsoul', realName: 'Hillary Gibbs', asset: 'Shifting Rainshadow Iron',}
  ];
  const ascCardData: CardData[] = [{
    playerName: 'Crystaldash',
    realName: 'Darcy Candice Ball',
    asset: 'Secret Glowquake Gold',
  },
    {playerName: 'Dreamlurk The Unstoppable', realName: 'Brianna Forbes', asset: 'Foghammer Lead',},
    {playerName: 'Speedsoul', realName: 'Hillary Gibbs', asset: 'Shifting Rainshadow Iron',}
  ];
  const descCardData: CardData[] = [{
    playerName: 'Speedsoul',
    realName: 'Hillary Gibbs',
    asset: 'Shifting Rainshadow Iron',
  },
    {playerName: 'Dreamlurk The Unstoppable', realName: 'Brianna Forbes', asset: 'Foghammer Lead',},
    {playerName: 'Crystaldash', realName: 'Darcy Candice Ball', asset: 'Secret Glowquake Gold',}

  ];

  it('should transform initCardData to ascCardData', () => {
    expect(pipe.transform(initCardData, 'ASC')).toEqual(ascCardData);
  });

  it('should transform initCardData to descCardData', () => {
    expect(pipe.transform(initCardData, 'DESC')).toEqual(descCardData);
  });
});
