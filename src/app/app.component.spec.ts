import {TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {SortPipe} from "./pipe/sort.pipe";
import {Order} from "./models/order";
import {CardData} from "./models/card-data";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ], providers: [SortPipe]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'AoeCardGame'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('AoeCardGame');
  });

  it('should set Order to orderChange parameter', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const paramOrder: Order = {name: 'Sort DESC', class: '', icon: 'sort', order: 'DESC'};
    app.orderChange(paramOrder);
    expect(app.order).toEqual(paramOrder);
  });


  it('should set Card to selectCard parameter', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const paramCard: CardData = {
      playerName: 'Dreamlurk The Unstoppable',
      realName: 'Brianna Forbes',
      asset: 'Foghammer Lead',
    };
    app.selectCard(paramCard);
    expect(app.card).toEqual(paramCard);
  });

});
