import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import DataService from '../data.service';
import {CardData} from '../models/card-data';
import {Order} from '../models/order';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.css']
})
export class ControlsComponent {

  @Output() orderChange: EventEmitter<Order> = new EventEmitter;
  @Input() card: CardData = {playerName: "", asset: "", realName: ""};
  orders: Order[] = [{name: 'Sort DESC', class: '', icon: 'sort', order: 'DESC'},
    {name: 'Sort ASC', class: 'reverted', icon: 'sort', order: 'ASC'}
  ];

  constructor(private dataService: DataService, private _snackBar: MatSnackBar) {
  }

  openSnackBar(message: string): void {
    this._snackBar.open(message, 'Close');
  }

  orderChanged(order: Order): void {
    this.orderChange.emit(order);
  }

  submit(): void {
    this.dataService.submitCard(this.card).toPromise().then(value => {
      if (value.playerName != "") {
        this.openSnackBar(value.playerName + ' Sent!');
      } else {
        this.openSnackBar('Nothing Sent!');
      }

    }).catch(reason => {
      this.openSnackBar('No response from Backend!');
      console.error(reason.message);
    });
  }

}

