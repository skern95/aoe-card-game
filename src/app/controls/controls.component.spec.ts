import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ControlsComponent} from './controls.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Overlay} from "@angular/cdk/overlay";
import {Order} from "../models/order";

describe('ControlsComponent', () => {
  let component: ControlsComponent;
  let fixture: ComponentFixture<ControlsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ControlsComponent],
      imports: [HttpClientTestingModule],
      providers: [MatSnackBar, Overlay]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit on click', () => {
    const fixture = TestBed.createComponent(ControlsComponent);
    const component = fixture.componentInstance;
    spyOn(component.orderChange, 'emit');
    const order: Order = {name: 'Sort DESC', class: '', icon: 'sort', order: 'DESC'};

    component.orderChanged(order);
    expect(component.orderChange.emit).toHaveBeenCalledWith(order);
  });
});
