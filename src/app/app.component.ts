import {Component} from '@angular/core';
import {CardData} from './models/card-data';
import {Order} from './models/order';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AoeCardGame';
  order: Order | undefined;
  card: CardData = {playerName: "", asset: "", realName: ""};

  orderChange(order: Order): void {
    this.order = order;
  }

  selectCard(card: CardData): void {
    this.card = card;
  }
}
