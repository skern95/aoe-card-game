import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OverviewComponent} from './overview.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {SortPipe} from "../pipe/sort.pipe";
import {CardData} from "../models/card-data";

describe('OverviewComponent', () => {
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OverviewComponent, SortPipe],
      imports: [HttpClientTestingModule],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call emit', () => {
    spyOn(component.selectedCard, 'emit');
    const card: CardData = {
      playerName: 'Dreamlurk The Unstoppable',
      realName: 'Brianna Forbes',
      asset: 'Foghammer Lead'
    };

    component.cardSelected(card);
    expect(component.selectedCard.emit).toHaveBeenCalledWith(card);
  });

  it('should call getCards', () => {
    spyOn(component['dataService'], 'getCardData');

    component.getCardData();
    expect(component['dataService'].getCardData).toHaveBeenCalled();
  });
});
