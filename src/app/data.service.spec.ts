import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';

import DataService from './data.service';
import {CardData} from "./models/card-data";
import {HttpResponse} from "@angular/common/http";

describe('DataService', () => {
  let service: DataService;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AppComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should http GET cards', () => {

    const cards: CardData[] = [
      {
        playerName: "Dreamlurk The Unstoppable",
        realName: "Brianna Forbes",
        asset: "Foghammer Lead"
      },
      {
        playerName: "Crystaldash",
        realName: "Darcy Candice Ball",
        asset: "Secret Glowquake Gold"
      },
      {
        playerName: "Speedsoul",
        realName: "Hillary Gibbs",
        asset: "Shifting Rainshadow Iron"
      },
      {
        playerName: "Seekvenom The Mystic",
        realName: "Elva Becky Hammond",
        asset: "Valkyries' Opal Adamant"
      },
      {
        playerName: "Coincurse The Ghoul ",
        realName: "Enid Rose",
        asset: "Jewelevil Bronze Of Goddesses"
      },
      {
        playerName: "Skulldart",
        realName: "Esmeralda Carrillo",
        asset: "Yellow Orichalcum Of Paladins"
      }
    ];


    service.getCardData().subscribe((res) => {
      expect(res).toEqual(cards);
    });

    const req = httpMock.expectOne('assets/card-data.json');
    expect(req.request.method).toEqual("GET");
    req.flush(cards);
  });

  it('should http POST a card and return it', () => {
    const card: CardData = {
      playerName: 'Dreamlurk The Unstoppable',
      realName: 'Brianna Forbes',
      asset: 'Foghammer Lead'
    };

    service.submitCard(card).subscribe(
      data => expect(data).toEqual(card, 'should return the card'),
      fail
    );

    // submitCard() should have made one request to POST card
    const req = httpMock.expectOne(service.cardUrl);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(card);

    // Expect server to return the card after POST
    const expectedResponse = new HttpResponse({status: 201, statusText: 'Created', body: card});
    req.event(expectedResponse);
  });

});
