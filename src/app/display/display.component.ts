import {Component, Input} from '@angular/core';
import {CardData} from '../models/card-data';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent {

  @Input() card: CardData = {playerName: "", asset: "", realName: ""};

}
